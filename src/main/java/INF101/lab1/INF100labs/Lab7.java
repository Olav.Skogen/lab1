package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        ArrayList<ArrayList<Integer>> grid1 = new ArrayList<>();
        grid1.add(new ArrayList<>(Arrays.asList(11, 12, 13)));
        grid1.add(new ArrayList<>(Arrays.asList(21, 22, 23)));
        grid1.add(new ArrayList<>(Arrays.asList(31, 32, 33)));

            
}

    

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        ArrayList<Integer> list1 = new ArrayList<>();
        int x = 0;
        int y = 0;
        int z = sumrow(grid.get(0));
        int n = sumcol(grid, 0);
        
        
        while(y < grid.size()) {
            int sum = sumrow(grid.get(y));
            if (sum != z) {
                return false;
            }
            y++;
        }

       
        while(x < grid.get(0).size()) {
            int sum = sumcol(grid, x);
            if (sum != n) {
                return false;
            }
            x++;
        }    
        return true;
    }

    public static int sumrow(ArrayList<Integer> row) {
        int sum = 0;
        int col = 0;
        while (col < row.size()) {
            sum += row.get(col);
            col++;
        }
        return sum;
    }
    public static int sumcol(ArrayList<ArrayList<Integer>> grid, int colIndex) {
        int sum = 0;
        for (int i = 0; i < grid.get(0).size(); i++) {
            sum += grid.get(i).get(colIndex);
        }
        return sum; 
        
    }
}
