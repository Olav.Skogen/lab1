package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        multiplesOfSevenUpTo(49);
        multiplicationTable(3); 
        int answer = crossSum(123);
        System.out.println(answer);
    }

    public static void multiplesOfSevenUpTo(int n) {
        int i = 0;
        for (i = 7; (i % 7 == 0) && (i <= n); i =i + 7) {
            System.out.println(i);
        }
    }

    public static void multiplicationTable(int n) {
        int i = 0;
        int j = 0;
        for (i = 1; i <= n; i++) {
            System.out.print(i + ": ");
            for (j =1; j <= n; j++) {
                System.out.print(i * j + " ");
            }
            System.out.println("");
            
        }
    }

    public static int crossSum(int num) {
        int sum = 0;
        while (num > 0) {
            sum = sum + num % 10;
            num = num / 10;
        }
        return sum; 
    }

}