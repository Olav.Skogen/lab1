package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        List<Integer> list1 = Arrays.asList(1, 2, 3, 4);
        ArrayList<Integer> arrayList1 = new ArrayList<>(list1);
        ArrayList<Integer> removedList1 = removeThrees(arrayList1);
        System.out.println(removedList1);
        
        List<Integer> liste2 = Arrays.asList(1, 1, 2, 1, 3, 3, 3, 2);
        ArrayList<Integer> arrayList2 = new ArrayList<>(liste2);
        ArrayList<Integer> removedList2 = uniqueValues(arrayList2);
        System.out.println(removedList2);


       
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        ArrayList<Integer> liste1 = new ArrayList<>();
        int listSize = list.size();
        for (int i = 0; i < listSize; i++) {
            if (list.get(i) != 3) {
                liste1.add(list.get(i));
            }
        }
        return liste1; 
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> liste2 = new ArrayList<>();
        int listSize = list.size();
        for (int i = 0; i < listSize; i++) {
            if (!liste2.contains(list.get(i))) {
                liste2.add(list.get(i));
            }
        }
        return liste2;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        int listSize = a.size();
        for (int i = 0; i < listSize; i++) {
            a.set(i, (a.get(i)) + b.get(i));
        }
          
    }   

}