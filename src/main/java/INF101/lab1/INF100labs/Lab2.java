package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        findLongestWords("Game", "Action", "Champion");
        boolean answer = isLeapYear(2000);
        System.out.println(answer);
        boolean even = isEvenPositiveInt(-2);
        System.out.println(even);
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        int lWord1 = word1.length();
        int lWord2 = word2.length(); 
        int lWord3 = word3.length();
        if (lWord1 >= lWord2 && lWord1 >= lWord3) {
            System.out.println(word1);
        }
        if (lWord2 >= lWord1 && lWord2 >= lWord3) {
            System.out.println(word2);
        }
        if (lWord3 >= lWord1 && lWord3 >= lWord2) {
            System.out.println(word3);
        }
        


    }

    public static boolean isLeapYear(int year) {
        if ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0)) )  {
            return true;
        } 

        else {
            return false;
        }
        

    }

    public static boolean isEvenPositiveInt(int num) {
        if ((num % 2 == 0) && (num >= 0)) {
            return true;
        }
        else {
            return false;
        }
    }

}
